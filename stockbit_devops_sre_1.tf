
#####
# VPC
#####

resource "aws_vpc" "main" {
  cidr_block       = "192.168.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "main"
  }
}

#####
# Public Subnet
#####

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "192.168.1.0/24"
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "Public Subnet"
  }
}

#####
# Private Subnet
#####

resource "aws_subnet" "private_nated" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "192.168.2.0/24"
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "Private Subnet with NAT"
  }
}

#####
# NAT Gateway
#####

resource "aws_eip" "nat_gw_eip" {
  vpc = true
}

resource "aws_nat_gateway" "gw" {
  allocation_id = aws_eip.nat_gw_eip.id
  subnet_id     = aws_subnet.public.id
}

#####
# Route Table
#####

resource "aws_route_table" "my_vpc_ap_southeast_1a_nated" {
    vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.gw.id
    }

    tags = {
        Name = "Main Route Table for NAT-ed subnet"
    }
}

resource "aws_route_table_association" "my_vpc_ap_southeast_1a_nated" {
    subnet_id = aws_subnet.private_nated.id
    route_table_id = aws_route_table.my_vpc_ap_southeast_1a_nated.id
}

#####
# Launch Configuration
#####

resource "aws_launch_configuration" "app" {
  name_prefix = "app-"

  image_id = "ami-0947d2ba12ee1ff75" # Amazon Linux 2 AMI (HVM), SSD Volume Type
  instance_type = "t2.medium"
}

#####
# Auto Scaling Group
#####

resource "aws_autoscaling_group" "app" {
  name = "${aws_launch_configuration.app.name}-asg"

  min_size             = 2
  desired_capacity     = 2
  max_size             = 5
  
  launch_configuration = aws_launch_configuration.app.name

  vpc_zone_identifier  = [
    aws_subnet.private_nated.id
  ]

  lifecycle {
    create_before_destroy = true
  }
}
